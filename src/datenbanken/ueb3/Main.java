package datenbanken.ueb3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;

public class Main extends Application
{

    @Override
    public void start( Stage primaryStage ) throws Exception
    {
        Parent root = FXMLLoader.load( getClass().getResource( "gui/gui.fxml" ) );
        primaryStage.setTitle( "\"Datenbanken 2\" Example Application By Falx" );
        primaryStage.setScene( new Scene( root, 800, 600 ) );
        primaryStage.show();
    }


    public static void main( String[] args ) throws SQLException
  {
/*        ConnectionWrapper wrapper = ConnectionWrapper.createInstance( DBDriver.ORACLE_XE, "xe", *//* insert username and password here *//* );
        System.out.println( wrapper.getConnection() );
        DepartmentDAO departmentDAO = new DepartmentDAO( wrapper.getConnection() );
        System.out.println( departmentDAO.findById( 1 ).getDeptMgr() );*/


        launch( args );
    }
}
