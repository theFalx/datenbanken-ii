package datenbanken.ueb3.dao;

import datenbanken.ueb3.Employee;

import java.util.List;

/**
 * @author falx
 * @version v0.1a
 */
public interface IEmployeeDAO extends IDataAccessObject<Employee>
{
    List<Employee> findByDepartmentId( int id );
}
