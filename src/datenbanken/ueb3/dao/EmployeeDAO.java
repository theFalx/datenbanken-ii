package datenbanken.ueb3.dao;

import datenbanken.ueb3.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author falx
 * @version v0.1a
 */
public class EmployeeDAO implements IEmployeeDAO
{
    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------------- //
                                            //   static declarations  //
                                            // ---------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    // -------------------------------------------- SQL Queries------------------------------------------------------ //

    private static final String GENERAL_SELECT = "SELECT e.employee_id, e.first_name, e.last_name, e.email\n" +
                                                 "FROM EMPLOYEES e\n" +
                                                 "%s\n";

    private static final String BY_ID = "WHERE e.employee_id = ?";
    private static final String ALL = "";
    private static final String BY_NAME = "WHERE e.last_name = ?";
    private static final String BY_DEPT_ID = "WHERE e.department_id = ?";

    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------------- //
                                            //   member declarations  //
                                            // ---------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    private final Connection mfConnection;

    // -------------------------------------------------------------------------------------------------------------- //
                                                // -------------- //
                                                //   Constructor  //
                                                // -------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public EmployeeDAO( Connection connection )
    {
        this.mfConnection = connection;
    }

    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------- //
                                            //  private methods //
                                            // ---------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    // parses all employees from the resultset and returns them in a list, or null if the resultSet was empty
    private List<Employee> parseEmployee( ResultSet result )
    {
        List<Employee> list = new ArrayList<>();
        try
        {
            while( result.next() )
                list.add( new Employee( result.getInt( 1 ), result.getString( 2 ), result.getString( 3 ), result.getString ( 4 ) ) );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }

        if( list.size() == 0 )
            return null;
        else
            return list;
    }

    // calls parseEmployee and return the first employee from the given list or null if the resultSet was empty
    private Employee parseSingleEmployee( ResultSet result )
    {
        List<Employee> list = parseEmployee( result );
        if( list == null )
            return null;
        else
            return list.get( 0 );
    }

    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------- //
                                            //  public methods  //
                                            // ---------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    // ----------------------------------------- overridden methods ------------------------------------------------- //

    @Override
    public Employee findById( int id )
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, BY_ID ) ) )
        {
            statement.setInt( 1, id );
            ResultSet result = statement.executeQuery();
            return parseSingleEmployee( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Employee> findAll()
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, ALL ) ) )
        {
            ResultSet result = statement.executeQuery();
            return parseEmployee( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public Employee findByName( String name )
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, BY_NAME ) ) )
        {
            statement.setString( 1, name );
            ResultSet result = statement.executeQuery();
            return parseSingleEmployee( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Employee> findByDepartmentId( int id )
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, BY_DEPT_ID ) ) )
        {
            statement.setInt( 1, id );
            ResultSet result = statement.executeQuery();
            return parseEmployee( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean insert( Employee object )
    {
        return false;
    }

    @Override
    public boolean update( Employee object )
    {
        return false;
    }

    @Override
    public boolean delete( Employee object )
    {
        return false;
    }
}
