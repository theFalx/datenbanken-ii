package datenbanken.ueb3.dao;

import datenbanken.ueb3.Department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: add method for generale resultset to department creation
 * TODO: add test to get if the resulset is empty!
 *
 * @author falx
 * @version v0.1a
 */
public class DepartmentDAO implements IDataAccessObject<Department>
{
    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------------- //
                                            //   static declarations  //
                                            // ---------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    // -------------------------------------------- SQL Queries------------------------------------------------------ //

    private static final String GENERAL_SELECT = "SELECT d.department_id, d.department_name, dept_manager(d.department_id), COUNT(e.employee_id)\n" +
                                                 "FROM Departments d\n" +
                                                 "JOIN Employees e ON e.department_id = d.department_id\n" +
                                                 "%s\n" +
                                                 "GROUP BY d.department_id, d.department_name";

    private static final String BY_ID = "WHERE d.department_id = ?\n";

    private static final String ALL = " ";

    private static final String BY_NAME = "WHERE d.department_name = ?\n";

    // -------------------------------------------------------------------------------------------------------------- //
                                              // -------------------- //
                                              //  member declaration  //
                                              // -------------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    private final Connection mfConnection;

    // -------------------------------------------------------------------------------------------------------------- //
                                                // ------------- //
                                                //  Constructor  //
                                                // ------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    public DepartmentDAO( Connection connection )
    {
        this.mfConnection = connection;
    }


    // -------------------------------------------------------------------------------------------------------------- //
                                                // ----------------- //
                                                //  private methods  //
                                                // ----------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    private List<Department> parseDepartment( ResultSet result )
    {
        List<Department> list = new ArrayList<>();
        try
        {
            while( result.next() )
                list.add( new Department( result.getInt( 1 ), result.getString( 2 ), result.getString( 3 ), result.getInt( 4 ) ) );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
            return null;
        }

        if( list.size() == 0 )
            return null;
        else
            return list;
    }

    private Department parseSingleDepartment( ResultSet result )
    {
        List<Department> list = parseDepartment( result );
        if( list == null )
            return null;
        else
            return list.get( 0 );
    }

    // -------------------------------------------------------------------------------------------------------------- //
                                            // ---------------- //
                                            //  public methods  //
                                            // ---------------- //
    // -------------------------------------------------------------------------------------------------------------- //

    // ---------------------------------------- overridden methods -------------------------------------------------- //

    @Override
    public Department findById( int id )
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, BY_ID ) ) )
        {
            statement.setInt( 1, id );
            ResultSet result = statement.executeQuery();
            return parseSingleDepartment( result );
        }
        catch( SQLException e )
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Department> findAll()
    {
        List<Department> list = null;
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, ALL )  ) )
        {
            ResultSet result = statement.executeQuery();
            list = parseDepartment( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }
        return list;
    }

    @Override
    public Department findByName( String name )
    {
        try( PreparedStatement statement = mfConnection.prepareStatement( String.format( GENERAL_SELECT, BY_NAME ) ) )
        {
            statement.setString( 1, name );
            ResultSet result = statement.executeQuery();
            return parseSingleDepartment( result );
        }
        catch( SQLException exc )
        {
            exc.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insert( Department object )
    {
        return false;
    }

    @Override
    public boolean update( Department object )
    {
        return false;
    }

    @Override
    public boolean delete( Department object )
    {
        return false;
    }
}
