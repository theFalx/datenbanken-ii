package datenbanken.ueb3.dao;

import java.util.List;

/**
 * Changed the name from DepartmentDAO to IDepartmentDAO because of naming conventions
 * @author fhws wuerzburg - Course Datenbanken II
 */

public interface IDataAccessObject<T>
{
    public T findById( int id );
    public List<T> findAll();
    public T findByName( String name );
    public boolean insert( T object );
    public boolean update( T object );
    public boolean delete( T object );
}
