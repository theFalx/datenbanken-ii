package datenbanken.ueb3;

/**
 * Class for departments.
 * @author fhws wuerzburg - course Datenbanken II
 * NOTE: NOT NECESSARY TO CHANGE
 */
public class Department {
    int deptID;
    String deptName;
    String deptMgr;
    int numEmp;
 
    public Department() {
        deptID = -1;
        deptName = null;
        deptMgr = null;
        numEmp = 0;
    }

    public Department(int deptID, String deptName, String deptMgr, int numEmp) {
        this.deptID = deptID;
        this.deptName = deptName;
        this.deptMgr = deptMgr;
        this.numEmp = numEmp;
    }

    public int getDeptID() {
        return deptID;
    }
    
    public void setDeptID(int deptID) {
        this.deptID = deptID;
    }
    
    public String getDeptName() {
        return deptName;
    }
    
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
    
    public String getDeptMgr() {
        return deptMgr;
    }
    
    public void setDeptMgr(String deptMgr) {
        this.deptMgr = deptMgr;
    }
    
    public int getNumEmp() {
        return numEmp;
    }
    
    public void setNumEmp(int numEmp) {
        this.numEmp = numEmp;
    }
    
    public String toString () {
        return deptID + ": " + deptName + " (" + numEmp + ")";
    }
}
