package datenbanken.ueb3;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Class for employees.
 *
 * @falx
 */
public class Employee
{

    private IntegerProperty mEmpIdProperty;
    private StringProperty mEmpIdStringProperty;
    private StringProperty mFirstNameProperty;
    private StringProperty mLastNameProperty;
    private StringProperty mEmailProperty;

    public Employee()
    {
        this( -1, "", "", "" );
    }

    public Employee( int empID, String firstName, String lastName, String email )
    {
        this.mEmpIdProperty = new SimpleIntegerProperty( empID );
        this.mEmpIdStringProperty = new SimpleStringProperty( String.valueOf( empID ) );
        this.mFirstNameProperty = new SimpleStringProperty( firstName );
        this.mLastNameProperty = new SimpleStringProperty( lastName );
        this.mEmailProperty = new SimpleStringProperty( email );
    }

    public int getEmpID()
    {
        return mEmpIdProperty.get();
    }

    public String getEmpIDString()
    {
        return mEmpIdStringProperty.get();
    }

    public IntegerProperty getEmpIDProperty()
    {
        return mEmpIdProperty;
    }

    public StringProperty getEmpIdStringProperty()
    {
        return mEmpIdStringProperty;
    }

    public void setEmpID( int empID )
    {
        mEmpIdProperty.set( empID );
        mEmpIdStringProperty.set( String.valueOf( empID ) );
    }

    public String getFirstName()
    {
        return mFirstNameProperty.get();
    }

    public StringProperty getFirstNameProperty()
    {
        return mFirstNameProperty;
    }

    public void setFirstName( String firstName )
    {
        mFirstNameProperty.set( firstName );
    }

    public String getLastName()
    {
        return mLastNameProperty.get();
    }

    public StringProperty getLastNameProperty()
    {
        return mLastNameProperty;
    }

    public void setLastName( String lastName )
    {
        mLastNameProperty.set( lastName );
    }

    public String getEmail()
    {
        return mEmailProperty.get();
    }

    public StringProperty getEmailProperty()
    {
        return mEmailProperty;
    }

    public void setEmail( String email )
    {
        mEmailProperty.set( email );
    }

    public String toString()
    {
        return "Employee(" + getEmpID() + ", " + getFirstName() + " " + getLastName() + ", " + getEmail() + ")";
    }
}
