package datenbanken.ueb3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * @author falx
 * @version v0.1a
 */
public class ConnectionWrapper
{
    public static enum DBDriver
    {
        ORACLE_XE( "oracle.jdbc.OracleDriver","jdbc:oracle:thin:@//localhost:1521/" );

        String fDriver;
        String fUrl;

        private DBDriver( String driver, String url )
        {
            this.fDriver = driver;
            this.fUrl = url;
        }
    }

    private final Connection mfConnection;


    /**
     * UserName and Password saved as char-array for more security. They will be erased after creating the object
     * @param driver defines the database used and the driver which is needed for it
     * @param database name of the database to connect
     * @param userName username used to connect
     * @param password password used to connect
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public ConnectionWrapper( DBDriver driver, String database, char[] userName, char[] password ) throws ClassNotFoundException, SQLException
    {
        Class.forName( driver.fDriver );
        String url = driver.fUrl + database;
        mfConnection = DriverManager.getConnection( url, new String( userName ), new String( password ) );
        // erase data after connecting
        Arrays.fill( userName, '\0' );
        Arrays.fill( password, '\0' );
    }


    /**
     * Wrapper for the Constructor ConnectionWrapper {@see datanbanken.ueb3.ConnectionWrapper}
     * @param driver
     * @param database
     * @param userName
     * @param password
     * @return a new ConnectionWrapper or null if an exception occurred
     */
    public static ConnectionWrapper createInstance( DBDriver driver, String database, char[] userName, char[] password )
    {
        try
        {
            return new ConnectionWrapper( driver, database, userName, password );
        }
        catch( ClassNotFoundException | SQLException exc )
        {
            exc.printStackTrace();
            return null;
        }
    }


    /** @return the Connection-object provided by this ConnectionWrapper instance */
    public Connection getConnection()
    {
        return mfConnection;
    }

    /** {@see java.lang.Object */
    @Override
    public void finalize() throws Throwable
    {
        if( mfConnection != null )
            mfConnection.close();
    }
}
