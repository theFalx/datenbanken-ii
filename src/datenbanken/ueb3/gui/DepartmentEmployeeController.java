package datenbanken.ueb3.gui;

import datenbanken.ueb3.ConnectionWrapper;
import datenbanken.ueb3.Department;
import datenbanken.ueb3.Employee;
import datenbanken.ueb3.dao.DepartmentDAO;
import datenbanken.ueb3.dao.EmployeeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author falx
 * @version v0.1a
 */
public class DepartmentEmployeeController implements Initializable
{
    // -------------------------------------------- fxml variables -------------------------------------------------- //

    @FXML private ComboBox  deptComboBox;
    @FXML private Button    deptNameUpdateBtn;
    @FXML private TextField deptNameUpdateIn;

    @FXML private TableView<Employee> employeeTableView;
    @FXML private TableColumn<Employee, String> employeeIdColumn;
    @FXML private TableColumn<Employee, String> employeeFirstNameColumn;
    @FXML private TableColumn<Employee, String> employeeLastNameColumn;
    @FXML private TableColumn<Employee, String> employeeEMailColumn;

    // ------------------------------------------- member variables ------------------------------------------------- //

    private DepartmentDAO mDepartmentDAO;
    private EmployeeDAO   mEmployeeDAO;

    private ConnectionWrapper mConnectionWrap;

    private ObservableList<Employee> mActiveEmployeesList;


    // ------------------------------------------- Initialize method ------------------------------------------------ //

    @Override
    public void initialize( URL url, ResourceBundle resourceBundle )
    {
        this.mConnectionWrap = ConnectionWrapper.createInstance( ConnectionWrapper.DBDriver.ORACLE_XE, "xe",
                                                                    "hr".toCharArray(), "hr".toCharArray() );
        this.mDepartmentDAO = new DepartmentDAO( mConnectionWrap.getConnection() );
        this.mEmployeeDAO   = new EmployeeDAO( mConnectionWrap.getConnection() );

        //this.employeeIdColumn.setCellValueFactory( new PropertyValueFactory<Employee, String>( "mEmpIdProperty" ) ); // problem? IntegerProperty used as StringProperty!?
        this.employeeIdColumn.setCellValueFactory( cellData -> cellData.getValue().getEmpIdStringProperty() ); // problem? IntegerProperty used as StringProperty!?
        //this.employeeFirstNameColumn.setCellValueFactory( new PropertyValueFactory<Employee, String>( "mFirstNameProperty" ) );
        this.employeeFirstNameColumn.setCellValueFactory( cellData -> cellData.getValue().getFirstNameProperty() );
        //this.employeeLastNameColumn.setCellValueFactory( new PropertyValueFactory<Employee, String>( "mLastNameProperty" ) );
        this.employeeLastNameColumn.setCellValueFactory( cellData -> cellData.getValue().getLastNameProperty() );
        //this.employeeEMailColumn.setCellValueFactory( new PropertyValueFactory<Employee, String>( "mEmailProperty" ) );
        this.employeeEMailColumn.setCellValueFactory( cellData -> cellData.getValue().getEmailProperty() );

        mActiveEmployeesList = FXCollections.observableArrayList(  );
        this.employeeTableView.setItems( mActiveEmployeesList );

        updateDeptComboBox();
    }

    // ------------------------------------------- class methods ---------------------------------------------------- //

    private void updateDeptComboBox()
    {
        List<Department> list = mDepartmentDAO.findAll();
        deptComboBox.getItems().addAll( list );
        deptComboBox.getSelectionModel().selectFirst();
    }

    private void updateEmployeeTable( Department department )
    {
        mActiveEmployeesList.removeAll( mActiveEmployeesList );
        mActiveEmployeesList.addAll( mEmployeeDAO.findByDepartmentId( department.getDeptID() ));
    }

    // -------------------------------------------- fxml methods ---------------------------------------------------- //

    @FXML
    public void updateDepartmentNameAction( ActionEvent actionEvent )
    {

    }


    @FXML
    public void departmentSelectedAction( ActionEvent actionEvent )
    {
        updateEmployeeTable( ((ComboBox<Department>)actionEvent.getSource()).getSelectionModel().getSelectedItem() );
    }
}
